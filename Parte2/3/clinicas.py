
class Clinicas:
    def __init__(self, clinica_nombre):
        self.clinica_nombre = clinica_nombre

    def atualizar(self):
        self.clinica_nombre = input("Nuevo nombre: ")

    def ver(self):
        print("Clínica")
        print(self.clinica_nombre)

    def eliminar(self):
        self.clinica_nombre = ""


class Citas:
    def __init__(self, paciente, fecha, medico, clinica,):
        self.paciente = paciente
        self.fecha = fecha
        self.medico = medico
        self.clinica = clinica
        print("Cita creada.\n")

    def atualizar(self):
        self.paciente = input("Nuevo paciente: ")
        self.fecha = input("Nueva fecha: ")
        self.medico = input("Nuevo médico: ")
        self.clinica = input("Nueva clínica: ")

    def ver(self):
        print("Paciente | Fecha | Médico | Clínica")
        print(f"{self.paciente} | {self.fecha} | {self.medico} | {self.clinica}")

    def eliminar(self):
        self.paciente = ""
        self.fecha = ""
        self.medico = ""
        self.clinica = ""
        print("Cita eliminada.\n")


class Pacientes:
    def __init__(self, paciente_nombre):
        self.paciente_nombre = paciente_nombre

    def atualizar(self):
        self.paciente_nombre = input("Nuevo nombre: ")

    def ver(self):
        print("Paciente")
        print(self.paciente_nombre)

    def eliminar(self):
        self.paciente_nombre = ""


class Medicos:
    def __init__(self, medico_nombre):
        self.medico_nombre = medico_nombre

    def atualizar(self):
        self.medico_nombre = input("Nuevo nombre: ")

    def ver(self):
        print("Médico")
        print(self.medico_nombre)

    def eliminar(self):
        self.medico_nombre = ""
