from clinicas import *

if __name__ == '__main__':
    print("Sistema de Citas Medicas")

    # Menu
    menu = 0
    while menu != 5:
        print("1. Citas")
        print("2. Clinicas")
        print("3. Medicos")
        print("4. Pacientes")
        print("5. Cerrar")
        menu = int(input())

        # Citas
        if menu == 1:
            while menu != 5:
                print("--- Citas Médicas ---")
                print("1. Crear\n2. Buscar\n3. Atualizar\n4. Eliminar\n5. Cerrar")
                menu = int(input())
                if menu == 1:
                    paciente = input("Paciente: ")
                    fecha = input("Fecha: ")
                    medico = input("Médico: ")
                    clinica = input("Clínica: ")
                    cita = Citas(paciente, fecha, medico, clinica)
                elif menu == 2:
                    cita.ver()
                elif menu == 3:
                    cita.atualizar()
                elif menu == 4:
                    cita.eliminar()

        # Clinicas
        if menu == 2:
            while menu != 5:
                print("--- Clínicas ---")
                print("1. Crear\n2. Buscar\n3. Atualizar\n4. Eliminar\n5. Cerrar")
                menu = int(input())
                if menu == 1:
                    nombre = input("Nombre: ")
                    clinica = Clinicas(nombre)
                elif menu == 2:
                    clinica.ver()
                elif menu == 3:
                    clinica.atualizar()
                elif menu == 4:
                    clinica.eliminar()

        # Médicos
        if menu == 3:
            while menu != 5:
                print("--- Médicos ---")
                print("1. Crear\n2. Buscar\n3. Atualizar\n4. Eliminar\n5. Cerrar")
                menu = int(input())
                if menu == 1:
                    nombre = input("Nombre: ")
                    medico = Medicos(nombre)
                elif menu == 2:
                    medico.ver()
                elif menu == 3:
                    medico.atualizar()
                elif menu == 4:
                    medico.eliminar()

        # Pacientes
        if menu == 4:
            while menu != 5:
                print("--- Pacientes ---")
                print("1. Crear\n2. Buscar\n3. Atualizar\n4. Eliminar\n5. Cerrar")
                menu = int(input())
                if menu == 1:
                    nombre = input("Nombre: ")
                    paciente = Pacientes(nombre)
                elif menu == 2:
                    paciente.ver()
                elif menu == 3:
                    paciente.atualizar()
                elif menu == 4:
                    paciente.eliminar()
