class CalcComision:
    """Calculo de comisión"""
    venta1 = ""
    venta2 = ""
    venta3 = ""
    total = ""
    comision = ""

    def __init__(self):
        self.venta1 = int(input("Ingresa el valor de la venta 1: "))
        self.venta2 = int(input("Ingresa el valor de la venta 2: "))
        self.venta3 = int(input("Ingresa el valor de la venta 3: "))
        self.total = self.venta1 + self.venta2 + self.venta3
        self.comision = self.total * 0.1
        print("El total de las ventas es $", round(self.total, 2), "y la comisión es $", round(self.comision, 2))
