class CalcSalario:
    """Classe Calculo de Salario"""
    horas = ""
    salbruto = ""
    segsocial = ""
    segeducativo = ""
    salneto = ""

    def __init__(self):
        self.horas = int(input("Ingresa las horas trabajadas: "))

    def calculoSalario(self):
        self.salbruto = self.horas * 1.32
        print("Salario Bruto: $", round(self.salbruto, 2))
        self.segsocial = self.salbruto * 0.09
        print("Seguro Social: $", round(self.segsocial, 2))
        self.segeducativo = self.salbruto * 0.0125
        print("seguro Educativo: $", round(self.segeducativo, 2))
        self.salneto = self.salbruto - self.segsocial - self.segeducativo
        print("Salario Neto: $", round(self.salneto, 2))
